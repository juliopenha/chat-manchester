#!/usr/bin/env python3
import numpy as np

from socket import *
from threading import Thread
from matplotlib.pyplot import step, show, axis

def encript_manchester(msg):
	msg_original = msg
	manchester = ""
	saida_em_binario = ""
	print("#####\tMensagem original:\t\t", msg_original)
	bin_tmp = ' '.join('{0:08b}'.format(ord(x), 'b') for x in msg_original)
	msg_em_binario = bin_tmp.replace(" ","")

	print("#####\tMensagem original em binário:\t", msg_em_binario)

	for x in range(len(msg_em_binario)):
		if msg_em_binario[x] == "0":
			manchester += "01"
		if msg_em_binario[x] == "1":
			manchester += "10"
		#print(msg_em_binario[x]," -> ", manchester)
	print("#####\tMensagem original codificada:\t",manchester)
	data = []
	for x in range(len(msg_em_binario)):
		data.append(int(msg_em_binario[x]))
	xaxis = np.arange(0, len(msg_em_binario))
	yaxis = np.array(data)
	step(xaxis, yaxis, color='r', linewidth=2.0)
	#	xmin, xmax, ymin, ymax
	axis([0, len(msg_em_binario)+1, 0, 1])
	show()
	return manchester

def decript_manchester(msg):
	saida_em_binario = ""
	msg_saida = ""
	manchester = msg
	print("Mensagem original codificada:\t",manchester)
	for x in range(0, (len(manchester)), 2):
		if manchester[x] == "0" and manchester[x+1] == "1":
			saida_em_binario += "0"
		if manchester[x] == "1" and manchester[x+1] == "0":
			saida_em_binario += "1"
		
	print("Mensagem recebida em binário:\t",saida_em_binario)
	msg_saida += ''.join(chr(int(saida_em_binario[i*8:i*8+8],2)) for i in range(len(saida_em_binario)//8))
	#print("#####\tMensagem original decodificada:\t",msg_saida)
	return msg_saida
	

def receive():
	msg_in = client_socket.recv(BUFSIZ).decode("utf8")
	while True:
		msg_in = client_socket.recv(BUFSIZ).decode("utf8")
		msg = decript_manchester(msg_in)
		if msg == "{quit}":
		    client_socket.close()
		    break
		if not msg:
		    break
		print(msg_in)
    
def send():
	msg = input()
	client_socket.send(bytes(msg, "utf8")) # só pega o nome do cara
	while True:
		msg = input()
		client_socket.send(bytes(encript_manchester(msg), "utf8"))
		if msg == "{quit}":
			break


HOST = input('Enter host: ')
PORT = input('Enter port: ')
if not PORT:
    PORT = 33000
else:
    PORT = int(PORT)

BUFSIZ = 1024
ADDR = (HOST, PORT)

client_socket = socket(AF_INET, SOCK_STREAM)
client_socket.connect(ADDR)

receive_thread = Thread(target=receive)
send_thread = Thread(target=send)
receive_thread.start()
send_thread.start()
receive_thread.join()
send_thread.join()
